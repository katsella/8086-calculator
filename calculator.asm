;   Not: Islem bitiminden sonra, menuyu yazdirmak uzun suruyor her islem bitiminde menuyu tekrar yazdirmamak icin, 
; kodun en altindaki islemSonrasiSifirla yi 0 yapmaniz yeterli.

include 'emu8086.inc'

ORG    100h
ekranSifirla:
call clear_screen 

printn 'Islem Sec'
printn '1: Toplama' 
printn '2: Cikarma'
printn '3: Carpma'
printn '4: Bolme' 
printn '5: Virgullu Bol'
printn '6: And'
printn '7: Or'
printn '8: Xor'
printn '0: Ekran Temizle'
printn 'X: Cikis'

islemSec:
mov bp, 0 ; bolme isleminde virgullu kismin sayaci sifirlaniyor

hataliGiris:
printn 
print '# '    

lea    DI, secim        ; Alinacak verinin saklanacagi yer
mov    DX, secimBoyutu  ; Alinacak verinin maksimum miktari 
call   get_string
mov dx, 0  ; sifirlanmazsa bolme isleminde hata olusuyor
mov di, 0      
printn

cmp secim,'0' ; if(cx=='1') ekranSifirla
je ekranSifirla

cmp secim,'1' ; if(cx=='1') topla
je topla

cmp secim,'2' ; else if(cx=='2') cikar
je cikar 

cmp secim,'3' ; else if(cx=='3') carp
je carp

cmp secim,'4' ; else if(cx=='4') bol
je bol

cmp secim,'5' ; else if(cx=='5') virgulluBol
je virgulluBol 

cmp secim,'6' ; else if(cx=='6') and_islem
je and_islem

cmp secim,'7' ; else if(cx=='7') or_islem
je or_islem

cmp secim,'8' ; else if(cx=='8') xor_islem
je xor_islem

cmp secim,'x' ; else if(cx=='x') bitir
je bitir
  
cmp secim,'X' ; else if(cx=='X') bitir
je bitir

printn 'Hatali Giris'
jmp hataliGiris ; else yanlisIslem 
 
 
sayilariAl:
print 'Sayi1: '
call   scan_num       ; Okunan sayiyi CX e atiyor
printn
mov    sayi1, cx

print 'Sayi2: '
call   scan_num  
printn
mov    sayi2, cx
ret   ; sayilariAl i cagiran kisma geri donuyor 
 
 
ikilikYaz:
mov cx,16 
ikilikYazDongu:

shl bx,1       ; saga 1 bit kaydir 
jc ikilikCarryBir  ; kaydirma isleminde en soldaki bit CF flag'inde saklaniyor
putc '0'
ikilikCarryBir:

jnc ikilikCarrySifir
putc '1'
ikilikCarrySifir:

loop ikilikYazDongu 
putc 'b'
ret

hexYaz:        ; 4 er 4 er, bitleri and ile maskeledim .  
mov bx,ax
and bx,0f000h
shr bx,12      ; maskelenmis bitleri bl nin basina kadar kaydirdim.
call byteHexYaz

mov bx,ax
and bx,0f00h
shr bx,8
call byteHexYaz 

mov bx,ax
and bx,00f0h
shr bx,4
call byteHexYaz

mov bx,ax
and bx,000fh
call byteHexYaz 

putc 'h'
ret

byteHexYaz:
cmp bx,9
ja harf
add bx,48   ; eger maskelenmis bit 9 dan buyuk degil ise 48 ekliyerek ascii rakam elde ettim
putc  bl    ; 48 ascii kodu 1 e denk geliyor
ret
harf:
add bx,55   ; eger 9 dan buyuk ise 55 ekleyerek ascii harfleri elde ettim.
putc  bl    ; 65 ascii kodu A demek fakat elimizdeki maskelenmis veri 9 dan buyuk oldugu icin 55 ekledim.
ret
 
yaz:
print 'Sayi1:  '
mov bx, sayi1
call ikilikYaz
printn 
print 'Sayi2:  '
mov bx, sayi2 
call ikilikYaz
printn
print 'Ikilik: '
mov bx,ax
call ikilikYaz
printn
print 'Ondalik: '
call print_num      ; AX deki sayiyi yaz  
printn 
print 'Hex: '
call hexYaz 
virgulluBolmeSonu:
printn
cmp islemSonrasiSifirla,1 
jne islemSec
print 'Menuye donmek icin Entera bas.'
call scan_num
jmp ekranSifirla 




and_islem:
printn 'AND'
call sayilariAl
printn                     
mov ax, sayi1
and ax, sayi2 
jmp yaz

or_islem:
printn 'OR'
call sayilariAl
printn                     
mov ax, sayi1
or ax, sayi2 
jmp yaz

xor_islem:
printn 'XOR'
call sayilariAl
printn                     
mov ax, sayi1
xor ax, sayi2  
jmp yaz

topla:
printn 'TOPLA'
call sayilariAl                     
mov ax, sayi1
add ax, sayi2 ; ax=ax+sayi2
jmp yaz

cikar:
printn 'CIKAR'
call sayilariAl 
mov ax, sayi1
sub ax, sayi2 ; ax=ax-sayi2
jmp yaz

carp:
printn 'CARP'
call sayilariAl 
mov ax, sayi1
imul sayi2  ; ax=ax*sayi2
jmp yaz

bol:
printn 'BOL'
call sayilariAl
call sayi2Kontrol

cmp sayi1,0
jnl pozitifSayi1 

mov bx, sayi1 
call tumleyen
mov ax,bx
idiv sayi2 
mov bx, ax 
call tumleyen 
mov ax,bx
jmp yaz
pozitifSayi1:
mov ax, sayi1
idiv sayi2
jmp yaz
  
  
virgulluBol:
cmp bp,0            ; virgulden sonraki sayi miktarini bp de sayiyor.   
ja virgulHesap      ; eger virgul hesabi yapiliyorsa ax registerinda, bir onceki bolme isleminin kalani mevcuttur.
printn 'VIRGULLU BOL'
call sayilariAl
call sayi2Kontrol
mov ax, sayi1
cmp ax,0
jnl sayi1Positif 
mov bx,ax
call tumleyen
mov ax, bx
idiv sayi2
mov bx, ax
call tumleyen
mov ax, bx
jmp islemDevam 
virgulHesap:
sayi1Positif:
idiv sayi2          ; ax=ax/sayi2
islemDevam:          
cmp dx,0
ja ondalik
cmp bp,0 
je  yaz
call print_num 
jmp virgulluBolmeSonu

ondalik: 
cmp bp, 0
jne  sonucYazma
printn
print 'Sonuc: '
cmp ax,0
jne sonucYazma
mov bx, sayi2
xor bx, sayi1  ; eger isaretleri farkliysa negatif bir sayi oluyor.
cmp bx,0
jnl sonucYazma
putc '-'
sonucYazma:
call print_num 
cmp bp, 0
jne  virgulGec 
print ','
virgulGec: 
cmp bp, 5           ; Virgulden sonra basamak sayisi
jae  virgulluBolmeSonu 

cmp sayi2,0
jnl pozitifSayi2
mov bx,sayi2
call tumleyen
mov sayi2,bx
pozitifSayi2:

inc bp
mov ax, dx
mov cx, 10
mul cx
jmp virgulluBol

sayi2Kontrol:
cmp sayi2,0
jne sifirdanFarkli
print 'Sifira bolunemez'
jmp islemSec
sifirdanFarkli:
ret

tumleyen:
not bx
inc bx    
ret 


bitir:
ret

DEFINE_PRINT_NUM_UNS
DEFINE_PRINT_NUM
DEFINE_SCAN_NUM
DEFINE_CLEAR_SCREEN
DEFINE_GET_STRING
 
sayi1 dw 0
sayi2 dw 0        

secim DB 2 DUP (0)    
secimBoyutu = $-secim   

islemSonrasiSifirla db 1

end
